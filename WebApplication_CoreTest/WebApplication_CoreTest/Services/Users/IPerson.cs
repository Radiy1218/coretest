﻿
using WebApplication_CoreTest.Services.Greetings;

namespace WebApplication_CoreTest.Services.Users
{
    public interface IPerson
    {
        IGreeter Greeter { get; set; }
    }

}
