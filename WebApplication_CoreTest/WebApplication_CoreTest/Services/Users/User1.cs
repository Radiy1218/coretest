﻿using WebApplication_CoreTest.Services.Greetings;

namespace WebApplication_CoreTest.Services.Users
{
    public class User1 : IUser1
    {
        public User1(IGreeter greeter)
        {
            this.Greeter = greeter;
        }

        public IGreeter Greeter { get; set; }
    }

}
