﻿using WebApplication_CoreTest.Services.Greetings;

namespace WebApplication_CoreTest.Services.Users
{
    public class User2 : IUser2
    {
        public User2(IGreeter greeter)
        {
            this.Greeter = greeter;
        }

        public IGreeter Greeter { get; set; }
    }

}
