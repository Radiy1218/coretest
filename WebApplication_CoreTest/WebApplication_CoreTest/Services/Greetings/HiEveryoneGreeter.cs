﻿
using Microsoft.Extensions.Logging;

namespace WebApplication_CoreTest.Services.Greetings
{
    public class HiEveryoneGreeter : IHiEveryoneGreeter
    {
        private const string Message = "Hi everyone!";//Can be extracted from storage (DB, file, etc.) but not needed by Technical task 

        //private readonly ILogger<HiEveryoneGreeter> _logger;

        //public HiEveryoneGreeter(ILogger<HiEveryoneGreeter> logger)
        //{
        //    _logger = logger;
        //}

        public HiEveryoneGreeter()
        {
        }

        public string SayHello()
        {
            //_logger.LogInformation($"HiEveryoneGreeter.SayHello called. Message: {Message}");

            return Message;
        }
    }
}
