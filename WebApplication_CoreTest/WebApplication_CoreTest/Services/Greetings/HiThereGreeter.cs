﻿
using Microsoft.Extensions.Logging;

namespace WebApplication_CoreTest.Services.Greetings
{
    public class HiThereGreeter : IHiThereGreeter
    {
        private const string Message = "Hi there!"; //Can be extracted from storage (DB, file, etc.) but not needed by Technical task 

        //private readonly ILogger<HiThereGreeter> _logger;

        //public HiThereGreeter(ILogger<HiThereGreeter> logger)
        //{
        //    _logger = logger;
        //}

        public HiThereGreeter()
        {
        }

        public string SayHello()
        {
           // _logger.LogInformation($"HiThereGreeter.SayHello called. Message: {Message}");

            return Message;
        }
    }
}
