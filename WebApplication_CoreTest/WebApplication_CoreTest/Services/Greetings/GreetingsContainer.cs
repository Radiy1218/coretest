﻿using Microsoft.Extensions.DependencyInjection;
using WebApplication_CoreTest.Services.Factory;

namespace WebApplication_CoreTest.Services.Greetings
{
    public static class GreetingsContainer
    {
        public static void ConfigureGreetingsService(this IServiceCollection services)
        {
            services.AddScoped<IHiThereGreeter, HiThereGreeter>();

            //services.AddScoped<IHiEveryoneGreeter, HiEveryoneGreeter>();
            services.AddFactory<IHiEveryoneGreeter, HiEveryoneGreeter>();
        }

    }
}
