﻿
using System;

namespace WebApplication_CoreTest.Services.Greetings
{
    public class GreeterFactory
    {
        public IGreeter GetGreeter(GreeterType type)
        {
            switch(type)
            {
                case GreeterType.HiEveryone:
                    return new HiEveryoneGreeter();
                case GreeterType.HiThere:
                    return new HiThereGreeter();
                default:
                    throw new ArgumentException("Unknown type");
            }
        }
    }
}
