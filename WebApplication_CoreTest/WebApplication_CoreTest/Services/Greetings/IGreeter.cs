﻿
namespace WebApplication_CoreTest.Services.Greetings
{
    public interface IGreeter
    {
        string SayHello();
    }
}
