﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication_CoreTest.Services.Factory
{
    public class BaseFactory<T> : IBaseFactory<T>
    {
        private readonly Func<T> _initFunc;

        public BaseFactory(Func<T> initFunc)
        {
            _initFunc = initFunc;
        }

        public T Create()
        {
            return _initFunc();
        }
    }

    public interface IBaseFactory<T>
    {
    }
}
