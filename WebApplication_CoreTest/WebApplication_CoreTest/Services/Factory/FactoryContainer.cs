﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace WebApplication_CoreTest.Services.Factory
{
    public static class FactoryContainer
    {
        public static void AddFactory<TService, TImplementation>(this IServiceCollection services)
            where TService : class
            where TImplementation : class, TService
        {
            services.AddTransient<TService, TImplementation>();
            services.AddSingleton<Func<TService>>(x => () => x.GetService<TService>());

            services.AddSingleton<IBaseFactory<TService>, BaseFactory<TService>>();
        }
    }
}
