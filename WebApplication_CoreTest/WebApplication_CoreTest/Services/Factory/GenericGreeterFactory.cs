﻿
using WebApplication_CoreTest.Services.Greetings;

namespace WebApplication_CoreTest.Services.Factory
{
    public class GenericGreeterFactory<T> where T: IGreeter, new()
    {
        public T CreateInstance()
        {
            return new T();
        }
    }
}
