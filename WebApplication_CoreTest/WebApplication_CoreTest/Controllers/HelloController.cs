﻿using Microsoft.AspNetCore.Mvc;
using WebApplication_CoreTest.Services.Greetings;
using WebApplication_CoreTest.Services.Users;

namespace WebApplication_CoreTest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HelloController : ControllerBase
    {
        private IGreeter _greeterService { get; set; }

        public HelloController(IHiEveryoneGreeter greeter)
        {
            _greeterService = greeter;
        }

        //Hi everyone!
        [HttpGet("")]
        public string Get()
        {
            IPerson user = new User2(_greeterService);
            return user.Greeter.SayHello();
        }
    }
}