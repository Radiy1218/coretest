﻿using Microsoft.AspNetCore.Mvc;
using WebApplication_CoreTest.Services.Greetings;
using WebApplication_CoreTest.Services.Users;

namespace WebApplication_CoreTest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HiController : ControllerBase
    {
        private IGreeter _greeterService { get; set; }

        public HiController(IHiThereGreeter greeter)
        {
            _greeterService = greeter;
        }

        //Hi there!
        [HttpGet("")]
        public string Get()
        {
            IPerson user = new User1(_greeterService);
            return user.Greeter.SayHello();
        }
    }
}