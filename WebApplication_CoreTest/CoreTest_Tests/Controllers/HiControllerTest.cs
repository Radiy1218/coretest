﻿using Moq;
using NUnit.Framework;
using System;
using WebApplication_CoreTest.Controllers;
using WebApplication_CoreTest.Services.Greetings;

namespace CoreTest_Tests.Controllers
{
    // HelloControllerTest will be almost the same
    [TestFixture]
    public class HiControllerTest
    {
        protected const string _expected = "Hi everyone!";
        protected Mock<IHiThereGreeter> _greeterMoc { get; set; }

        public HiControllerTest()
        {
            _greeterMoc = new Mock<IHiThereGreeter>();
            _greeterMoc
                .Setup(n => n.SayHello())
                .Returns(_expected);
        }

        [Test]
        public virtual void SayHello_NullException()
        {
            HiController controller = new HiController(null);
            Assert.Throws<NullReferenceException>(() => controller.Get());
        }

        [Test]
        public virtual void SayHello_Success()
        {
            HiController controller = new HiController(_greeterMoc.Object);

            string result = controller.Get();

            //Assert.AreEqual(Message, result);
            Assert.That(result, Is.EqualTo(_expected));
        }

        [Test]
        public virtual void SayHello_NotSuccess()
        {
            HiController controller = new HiController(_greeterMoc.Object);

            string result = controller.Get();
            Assert.AreNotEqual(null, result);
        }

    }
}
