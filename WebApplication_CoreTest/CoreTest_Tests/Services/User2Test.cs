﻿using Moq;
using NUnit.Framework;
using System;
using WebApplication_CoreTest.Services.Greetings;
using WebApplication_CoreTest.Services.Users;

namespace CoreTest_Tests.Services
{
    // User1Test will be almost the same
    [TestFixture]
    public class User2Test
    {
        protected const string _expected = "Hi everyone!";
        protected Mock<IHiThereGreeter> _greeterMoc { get; set; }

        public User2Test()
        {
            _greeterMoc = new Mock<IHiThereGreeter>();
            _greeterMoc
                .Setup(n => n.SayHello())
                .Returns(_expected);
        }

        [Test]
        public virtual void GreeterSayHello_NullException()
        {
            var person = new User2(null);
            Assert.Throws<NullReferenceException>(() => person.Greeter.SayHello());
        }

        [Test]
        public virtual void GreeterSayHello_Success()
        {
            User2 person = new User2(_greeterMoc.Object);
            string result = person.Greeter.SayHello();

            //Assert.AreEqual(_expected, result);
            Assert.That(result, Is.EqualTo(_expected));
        }

        [Test]
        public virtual void SayHello_NotSuccess()
        {
            User2 person = new User2(_greeterMoc.Object);
            string result = person.Greeter.SayHello();

            Assert.AreNotEqual(null, result);
        }
    }
}
