﻿using NUnit.Framework;
using WebApplication_CoreTest.Services.Greetings;

namespace CoreTest_Tests.Services
{
    public class HiThereGreeterTest : IGreeterGenericTest<IHiThereGreeter>
    {

        public HiThereGreeterTest()
        {
            Greeter = new HiThereGreeter();
        }

        [Test]
        [TestCase("Hi there!")]
        public override void SayHello_Success(string expected)
        {
            base.SayHello_Success(expected);
        }
    }
}
