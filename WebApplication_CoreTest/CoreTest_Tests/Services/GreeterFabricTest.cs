﻿using NUnit.Framework;
using System;
using WebApplication_CoreTest.Services.Greetings;

namespace CoreTest_Tests.Services
{
    [TestFixture]
    public class GreeterFabricTest
    {

        [Test]
        public virtual void SayHello_NullException()
        {
            Assert.Throws<ArgumentException>(() => new GreeterFactory().GetGreeter((GreeterType)999999));
        }

        [Test]
        ////[TestCase("Hi everyone!", GreeterType.HiEveryone)]
        ////[TestCase("Hi there!", GreeterType.HiThere)]
        public virtual void SayHello_Success([Values("Hi there!", "Hi everyone!")]string expected, [Values(GreeterType.HiThere, GreeterType.HiEveryone)]GreeterType type)
        {
            IGreeter greeter = new GreeterFactory().GetGreeter(type);
            if (greeter == null)
                Assert.Fail();

            string result = greeter.SayHello();
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        [TestCase(GreeterType.HiEveryone)]
        [TestCase(GreeterType.HiThere)]
        [TestCase((GreeterType)999999)]
        public virtual void SayHello_NotSuccess(GreeterType type)
        {
            IGreeter greeter = new GreeterFactory().GetGreeter(type);
            if (greeter == null)
                Assert.Fail();

            string result = greeter.SayHello();
            Assert.AreNotEqual(null, result);
        }

    }
}
