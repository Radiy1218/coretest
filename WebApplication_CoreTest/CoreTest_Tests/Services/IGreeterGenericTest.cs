﻿using NUnit.Framework;
using System;
using WebApplication_CoreTest.Controllers;
using WebApplication_CoreTest.Services.Greetings;

namespace CoreTest_Tests.Services
{
    [TestFixture]
    public abstract class IGreeterGenericTest<T> where T: IGreeter
    {
        protected T Greeter { get; set; }

        [Test]
        public virtual void SayHello_NullException()
        {
            HiController controller = new HiController(null);
            Assert.Throws<NullReferenceException>(() => controller.Get());
        }

        [Test]
        public virtual void SayHello_Success(string expected)
        {
            string result = Greeter.SayHello();
            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public virtual void SayHello_NotSuccess()
        {
            string result = Greeter.SayHello();
            Assert.AreNotEqual(null, result);
        }

    }
}
